Meteor.startup(function() {

  // Tracker.autorun is no longer available on the server
  if (Meteor.isClient) {
    return Tracker.autorun(function() {
      var args = Mediator.subscribe('notification');
      if (args) {
        return Notify.setMessage(args[0]);
      }
    });
  }
});

Notify = {
  setMessage: function(message) {}
};
