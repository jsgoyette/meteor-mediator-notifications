var stack_topright = {
  'dir1': 'down',
  'dir2': 'left',
  'push': 'top',
  'spacing1': 5
};

function get_center_pos(width, top) {
  // top is empty when creating a new notification
  // and is set when recentering
  if (!top) {
    top = 6;
    $('.notification').each(function() {
      top += $(this).outerHeight() + 6;
    });
  }

  return {
    "top": top,
    "left": ($(window).width() / 2) - (width / 2)
  }
}


Notify.setMessage = function(opts) {

  if (typeof opts === 'string') {
    opts = { text: opts };
  }

  var defaults = {
    title: '',
    text: '',
    type: 'success',
    icon: 'glyphicon glyphicon-exclamation-sign',
    // icon: 'picon picon-mail-unread-new',
    addclass: 'common notification',
    opacity: '.86',
    width: '260px',
    delay: 4000,
    animate_speed: 300,
    shadow: false,
    nonblock: {
      nonblock: true
    },
    stack: stack_topright,
    before_open: function(PNotify) {
      PNotify.get().css(get_center_pos(PNotify.get().width()));
    }
  };

  var message = _.extend(defaults, opts);

  // only show message if opts has a value
  if (!_.isEmpty(opts)) {
    return new PNotify(message);
  }

  // return console.log('CLIENT MESSAGE: ', message);

};

// $(document).ready(function() {
//   $(window).resize(function() {
//     $(".ui-pnotify").each(function() {
//       $(this).css(get_center_pos($(this).width(), $(this).position().top))
//     });
//   });
// });
